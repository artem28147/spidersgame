package spiders.model.events;

import java.util.EventListener;

/** Слушатель деятельности паука.
 * Реагирует на любую деятельность паука
*/
public interface SpiderActionListener extends EventListener {
    
    /** Реакция на ход паука
     * @param e событие
    */
    void spiderMakedMove(SpiderActionEvent e);
    
    /** Реакция на попытку съесть насекомое
     * @param e событие
    */
    void spiderTryingToEatInsect(SpiderActionEvent e);
    
    /** Реакция на съедение пауком насекомого
     * @param e событие
    */
    void spiderAteInsect(SpiderActionEvent e);
}
