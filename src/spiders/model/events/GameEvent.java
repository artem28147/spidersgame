package spiders.model.events;

import java.util.EventObject;
import spiders.model.WebDweller;

/** Событие, связанное с изменением состоянии игры.
 * Сигнализирует об изменении состояния игры 
*/
public class GameEvent extends EventObject {
    //Обитатель паутины, с которым связано событие
    WebDweller _dweller;
    int _playerPassedStepsCount;
    public GameEvent(Object source) { 
        super(source); 
    } 
    
    /** Установить количество шагов, проиденных игроком
     * @param playerPassedStepsCount обитатель паутины
    */
    public void setPlayerPassedStepsCount(int playerPassedStepsCount){
        _playerPassedStepsCount = playerPassedStepsCount;
    }
    
    /** Вернуть количество шагов, проиденных игроком
     * @return обитатель паутины
    */
    public int playerPassedStepsCount() { 
        return _playerPassedStepsCount;
    } 
    
    /** Установить обитателя паутины
     * @param dweller обитатель паутины
    */
    public void setDweller(WebDweller dweller){
        _dweller = dweller;
    }
    
    /** Вернуть обитателя паутины
     * @return обитатель паутины
    */
    public WebDweller dweller() { 
        return _dweller;
    } 
}
