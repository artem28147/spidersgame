package spiders.model.events;

import java.util.EventObject;
import spiders.model.Insect;
import spiders.model.Spider;

/** Событие, связанное с деятельностью паука.
 * Сигнализирует о любой деятельности паука
*/
public class SpiderActionEvent extends EventObject {
    //Потерянный пауком после попытки съесть насекомое уровень здоровья
    int _lostHealth;
    //Полученный пауком уровень здоровья
    int _receivedHealth;
    Spider _spider;
    Insect _victim;
    
    public SpiderActionEvent(Object source) { 
        super(source); 
    } 
    
    /** Установить паука
     * @param spider паук
    */
    public void setSpider(Spider spider){
        _spider = spider;
    }
    
    /** Установить полученный пауком уровень здоровья
     * @param hp уровень здоровья
    */
    public void setReceivedHealth(int hp){
        _receivedHealth = hp;
    }
    
    /** Установить потерянный пауком уровень здоровья
     * @param hp уровень здоровья
    */
    public void setLostHealth(int hp){
        _lostHealth = hp;
    }
    
    /** Установить насекомое, на которое напал паук
     * @param victim насекомое
    */
    public void setVictim(Insect victim){
        _victim = victim;
    }
    
    /** Вернуть полученный пауком уровень здоровья
     * @return уровень здоровья
    */
    public int receivedHealth() { 
        return _receivedHealth;
    } 
    
    /** Вернуть потерянный пауком уровень здоровья
     * @return уровень здоровья
    */
    public int lostHealth() { 
        return _lostHealth;
    } 
    
    
    /** Вернуть паука
     * @return уровень здоровья
    */
    public Spider spider() { 
        return _spider;
    } 
    
    /** Вернуть насекомое, на которое напал паук
     * @return уровень здоровья
    */
    public Insect victim() { 
        return _victim;
    } 
} 
