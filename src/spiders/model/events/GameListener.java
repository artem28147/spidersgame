package spiders.model.events;

import java.util.EventListener;

/** Слушатель состояния игры.
 * Реагирует на изменение состояния игры
*/
public interface GameListener extends EventListener {
    /** Реакция на конец игры
     * @param e событие
    */
    void gameOver(GameEvent e);
    /** Реакция на изменение ситуации на паутине
     * @param e событие
    */
    void webSituationChanged(GameEvent e);
}
