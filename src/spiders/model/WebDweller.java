package spiders.model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import spiders.model.navigation.NodePosition;
import spiders.view.GamePanel;

/** Обитатель паутины.
 * Находится на определенной позиции на паутине; Может умереть, при этом удалится с паутины;
 * имеет собственную иконку для отображения; 
*/
public class WebDweller {
    private NodePosition _position;
    protected boolean _isDead=false;
    protected Web _web;
    protected String _icon;
    
    /** Установить иконку
     * @param icon иконка
    */
    protected void setIcon(String icon){
        _icon = icon;
    }
    
    /** Получить иконку
     * @return иконка
    */
    public String icon (){
        return _icon;
    }
    
    /** Установить позицию
     * @param newPosition позиция
    */
    public void setPosition(NodePosition newPosition){
        if (!_isDead)
            _position = newPosition;
    }
    
    /** Умереть
    */
    public void die(){
        if (!_isDead)
        _isDead = true;
        _web.deleteWebDweller(this);
    }
    
    /** Вернуть, мертв ли данный обитатель паутины
     * @return true - если мертв, false - если не мертв
    */
    public boolean isDead(){
        return _isDead;
    }
    
    /** Вернуть позицию
     * @return позиция
    */
    public NodePosition position(){
        return _position;
    }
    
    /** Установить паутину
     * @param newWeb паутина
    */
    public void setWeb(Web newWeb){
        _web=newWeb;
    }

    /** Отрисовать обитателя паутины
     * @param g класс, обслуживающий графику
     * @param CELL_SIZE размер клетки
     * @param FONT_HEIGHT размер шрифта
     * @param panel панель, на которой отрисовывать
     * @param leftTop точка, указывающая позицию левого верхний угол узла паутины, на котором находится обитатель
     */
    public void draw(Graphics g, Point leftTop, int CELL_SIZE, int FONT_HEIGHT, GamePanel panel) {
        //Прочитать файл иконки обитателя паутины
        File sourceimage = new File(_icon);
        Image image = null;
        //Отрисовать иконку
        try {
            image = ImageIO.read(sourceimage);
        } catch (IOException ex) {
            Logger.getLogger(GamePanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        g.drawImage(image, leftTop.x + CELL_SIZE / 8 + 27, leftTop.y + CELL_SIZE / 4 + FONT_HEIGHT + 70, panel);
    }
}
