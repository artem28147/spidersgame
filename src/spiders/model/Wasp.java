package spiders.model;

import spiders.model.events.SpiderActionEvent;
import spiders.model.events.SpiderActionListener;

/** Насекомое оса.
 * Может укусить паука, который пытается её съесть
 */
public class Wasp extends Insect{
    //Сила укуса
    private int _attackStrength=1;
    
    /** Породить осу
    */
    public Wasp(){
        _size=3;
        super.setIcon("wasp.png");
    }
    
    /** Укусить паука с некоторым шансом
     * @param attacker паук
    */
    void tryBite (Spider attacker){
        //С шансом 0.5
        if(Math.random()<0.5)
            //Укус осы отнимает у атакующего паука здоровье в зависимости от её размера и силы укуса
            attacker.reduceHealth(super.size()*_attackStrength);
    }
    
    /** Реакция на действие паука
    */
    public class TryBiteBySpiderAction implements SpiderActionListener {
        Insect _parent;
        /** Отреагировать на событие "паук пытается съест насекомое"
         * @param e событие
        */
        @Override
        public void spiderTryingToEatInsect(SpiderActionEvent e) {
            if (e.victim() == _parent)
            tryBite(e.spider());
        }
        
        /** Отреагировать на событие "паук съел насекомое"
         * @param e событие
        */
        @Override
        public void spiderAteInsect(SpiderActionEvent e) {
        }
        
        /** Отреагировать на событие "паук сделал ход"
         * @param e событие
        */
        @Override
        public void spiderMakedMove(SpiderActionEvent e) {
        }
        
        /** Установить внешний класс
         * @param parent внешний класс
        */
        public void setParent(Insect parent) {
            _parent=parent;
        }
    }
}
