package spiders.model.navigation;

/** Направление.
 * Север, юг, восток, запад
*/
public class Direction {
    //Угол
    private int _angle = 90;

    /** Породить направление основываясь на угле
     * @param angle угол
    */
    private Direction(int angle) {
        angle = angle%360;
        if(angle < 0)    angle += 360;
        
        this._angle = angle;
    }
    
    /** Вернуть направление-север
     * @return направление-север
    */
    public static Direction north()
    { return new Direction(90); }
    
    /** Вернуть направление-юг
     * @return направление-юг
    */
    public static Direction south()
    { return new Direction(270); }
    
    /** Вернуть направление-восток
     * @return направление-восток
    */
    public static Direction east()
    { return new Direction(0); }
    
    /** Вернуть направление-запад
     * @return направление-запад
    */
    public static Direction west()
    { return new Direction(180); }
    
    /** Клонировать направление
     * @return результат клонирования
    */
    @Override
    public Direction clone(){ 
        return new Direction(this._angle); 
    }
  
    /** Проверить, равно ли направление другому
     * @param other другое направление
     * @return true - равно, false - не равно
    */
    @Override
    public boolean equals(Object other) {

        if(other instanceof Direction) {
            Direction otherDirect = (Direction)other;
            return  _angle == otherDirect._angle;
        }
        return false;
    }
}