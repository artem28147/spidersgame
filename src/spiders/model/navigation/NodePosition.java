/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spiders.model.navigation;

/** Позиция узла паутины.
 * Определяется строкой и столбцом
*/
public class NodePosition
{
    //Строка
    private int _row;
    //Столбец
    private int _column;
    
    /** Породить направление основываясь на строке и столбце
     * @param row строка
     * @param col столбец
    */
    public NodePosition(int row, int col)
    {
        _row = row;
        _column = col;
    }
    
    /** Вернуть строку
     * @return строка
    */
    public int row(){
        return _row;
    }
    
    /** Вернуть столбец
     * @return столбец
    */
    public int column(){
        return _column;
    }
    
    /** Клонировать позиция узла
     * @return результат клонирования
    */
    @Override
    public NodePosition clone(){
        return new NodePosition(_row, _column);
    }
    
    /** Проверить, равен ли узел другому
     * @param other другой узел
     * @return true - равен, false - не равен
    */
    @Override
    public boolean equals(Object other){
        if(other instanceof NodePosition) {
            // Типы совместимы, можно провести преобразование
            NodePosition otherPosition = (NodePosition)other;
            // Возвращаем результат сравнения столбцов и строк
            return _row == otherPosition._row && _column == otherPosition._column;
        }
        return false;
    }
}