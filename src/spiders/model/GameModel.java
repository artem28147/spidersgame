package spiders.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import spiders.model.events.GameEvent;
import spiders.model.events.GameListener;
import spiders.model.events.SpiderActionEvent;
import spiders.model.events.SpiderActionListener;
import spiders.model.navigation.NodePosition;
import spiders.model.Wasp.TryBiteBySpiderAction;

/** Модель игры.
 * Aбстракция всей игры; генерирует стартовую обстановку; следит за пауком игрока с целью определения конца игры; 
 * контролирует ситуацию на паутине; управляет пауками-ботами с помощью искусственного интеллекта
*/
public class GameModel {
    private Spider _playerSpider;
    private List<Spider> _botsSpiders;
    private List<Insect> _insects;
    private ArtificialIntelligence _ai;
    private Web _web;
    private List<Insect> _completeInsectsPool;
    
    /** Породить модель игры
    */
    public GameModel(){
        _web = new Web(11,11);
        _botsSpiders = new ArrayList<Spider>();
        _completeInsectsPool = new ArrayList<Insect>();
        _insects = new ArrayList<Insect>();
        _ai = new ArtificialIntelligence();
        _ai.setModel(this);
        
        //Заполнить список эталонов насекомых
        Fly fly = new Fly();
        Grasshopper grasshoper = new Grasshopper();
        Mosquito mosquito = new Mosquito();
        Wasp wasp = new Wasp();
        
        _completeInsectsPool.add(fly);
        _completeInsectsPool.add(grasshoper);
        _completeInsectsPool.add(mosquito);
        _completeInsectsPool.add(wasp);
       
    }
    
    /** Вернуть паука игрока
     * @return паук игрока
    */
    public Spider playerSpider (){
        return _playerSpider;
    }
    
    /** Вернуть список насекомых
     * @return неизменяемый список насекомых
    */
    public List<Insect> insects (){
        //получаем список насекомых на паутине
        _insects.clear();
        List<WebDweller> dwellers = _web.dwellers();
        for(WebDweller dweller : dwellers){
            if (dweller instanceof Insect){
                _insects.add((Insect)dweller);
            }
        }
        return Collections.unmodifiableList(_insects);
    }
    
    /** Вернуть паутину
     * @return паутина
    */
    public Web web (){
        return _web;
    }
    
    /** Определить конец игры, подать сигнал, если игра окончена
     * @return true - игра окончена, false - игра не окончена
    */
    private boolean identifyGameOver(){
        boolean gameOver = _playerSpider.isDead();
        if (gameOver){
            fireGameOver();
            _playerSpider=null;
        }
        return gameOver;
    }
    
    /** Инициализировать начало игры
    */
    public void start(){
        _insects.clear();
        _botsSpiders.clear();
        generateWeb();
    }

    /** Сгенерировать паутину, наполненную пауками и насекомыми
     * @return true - игра окончена, false - игра не окончена
    */
    private void generateWeb(){
        _web.clear();
        //Добавляем паука игрока
        _playerSpider = new Spider();
        _playerSpider.setIsPlayer(true);
        _playerSpider.addSpiderActionListener(new GameModel.UpdateWebByAction());
        NodePosition spiderPos = _web.centralPosition();
        _web.addWebDweller(_playerSpider, spiderPos);
        
        //Добавляем пауков-ботов
        for(int i=0; i<_web.height()*_web.width()/12; i++){
            Spider botSpider = new Spider();
            //Добавляем паука на случайный пустой узел паутины
            _web.addWebDweller(botSpider, _web.randomBlankNodePosition());
            _botsSpiders.add(botSpider);
        }
        
        //Добавляем насекомых
        addInsectsToTheWeb();
    }
    
    /** Добавить насекомых на паутину
    */
    private void addInsectsToTheWeb(){
        Insect currentInsect;
        while (_insects.size() < _web.height()*_web.width()/7){
            currentInsect=catchAnyInsect();
            //Если насекомое - оса, подключить её к прослушиванию всех пауков, чтобы она кусалась при попытке её съесть
            if (currentInsect instanceof Wasp){
                SpiderActionListener listeningWasp = ((Wasp)currentInsect).new TryBiteBySpiderAction();
                ((TryBiteBySpiderAction)listeningWasp)._parent=currentInsect;
                _playerSpider.addSpiderActionListener(listeningWasp);
                for (Spider bot : _botsSpiders){
                    bot.addSpiderActionListener(listeningWasp);
                }
            }
            _insects.add(currentInsect);
        }
    }

    /** Поймать любое насекомое в паутину
     * @return пойманное насекомое
     */
    public Insect catchAnyInsect() {
        Insect currentInsect = null;
        boolean caught = false;
        Random rand = new Random();
        //Клонировать список эталонов насекомых
        ArrayList<Insect> tmpInsectsPool = (ArrayList<Insect>) ((ArrayList) _completeInsectsPool).clone();
        //Пока в паутину не попало какое-либо насекомое
        while (!caught) {
            //Клонировать случайное насекомое из списка эталонов
            currentInsect = (Insect) tmpInsectsPool.get(rand.nextInt(tmpInsectsPool.size())).clone();
            currentInsect.setWeb(_web);
            //Попытаться поймать насекомое
            caught = currentInsect.tryToGetIntoTheWeb();
            //Если насекомое данного типа не попало в паутину, удалить его из списка эталонов
            ListIterator<Insect> insIter = ((ArrayList) tmpInsectsPool).listIterator();
            if (!caught) {
                while (insIter.hasNext()) {
                    if (insIter.next().getClass().equals(currentInsect.getClass())) {
                        insIter.remove();
                        break;
                    }
                }
            }
        }
        return currentInsect;
    }
    
    /** Реакция на действие паука игрока
    */
    public class UpdateWebByAction implements SpiderActionListener{

        /** Отреагировать на событие "паук сделал ход"
         * @param e событие
        */
        @Override
        public void spiderMakedMove(SpiderActionEvent e) {
            _playerSpider.increasePassedStepsCount();
            
            ListIterator<Spider> botsIter = ((ArrayList)_botsSpiders).listIterator();
            while(botsIter.hasNext()){
                _ai.makeMove(botsIter.next());
                if (botsIter.previous().isDead()){
                    botsIter.remove();
                }
                else botsIter.next();
            }
            
            for (Insect ins : insects())
                ins.tryEscape();
            
            addInsectsToTheWeb();
            fireWebSituationChanged();
            identifyGameOver();
        }
        
        /** Отреагировать на событие "паук пытается съесть насекомое"
         * @param e событие
        */
        @Override
        public void spiderTryingToEatInsect(SpiderActionEvent e) {
        }
        
        /** Отреагировать на событие "паук съел насекомое"
         * @param e событие
        */
        @Override
        public void spiderAteInsect(SpiderActionEvent e) {
        }
    }
    
    //Слушатели игры
    private ArrayList<GameListener> _listeners = new ArrayList();
    
    /** Добавить слушателя игры
     * @param l слушатель
    */
    public void addGameListener(GameListener l) {
        _listeners.add(l);
    }
    
    /** Удалить слушателя игры
     * @param l слушатель
    */
    public void removeGameListener(GameListener l){
        _listeners.remove(l);
    }
    
    /** Сигнализировать об окончании игры
    */   
    private void fireGameOver() {
        GameEvent event = new GameEvent(this);
        event.setPlayerPassedStepsCount(_playerSpider.passedStepsCount());
        for (GameListener listener : _listeners)
            listener.gameOver(event);   
    }
    /** Сигнализировать об изменении ситуации на паутине
    */   
    private void fireWebSituationChanged() {
        GameEvent event = new GameEvent(this);
        
        for (GameListener listener : _listeners)
            listener.webSituationChanged(event);   
    }
}
