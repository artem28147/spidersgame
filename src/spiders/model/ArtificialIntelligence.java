package spiders.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import spiders.model.navigation.Direction;

/** Искусственный интеллект.
 * Передвигает подконтрольного паука
 */
public class ArtificialIntelligence {
    private GameModel _model;
    
    /** Передвинуть подконтрольного паука в направлении ближайшего насекомого находящегося на одной линии с пауком,
     * если таких насекомых несколько, передвинуть в направлении ближайшего.
     * Если насекомых нет, передвинуть паука в случайном направлении.
     * @param controllable подконтрольный паук
    */
    public void makeMove(Spider controllable){
        Random rand = new Random();
        
        List<Insect> insects = _model.insects();
        
        List<Insect> insectsAtSpiderRow = new ArrayList<Insect>();
        List<Insect> insectsAtSpiderCol = new ArrayList<Insect>();
        
        for (Insect ins : insects){
            //Заполняем список насекомых находящихся на одном столбце с пауком
            if(ins.position().column()==controllable.position().column() )
                insectsAtSpiderCol.add(ins);
            //и на одной строке с пауком
            else if(ins.position().row()==controllable.position().row())
                insectsAtSpiderRow.add(ins);
        }
        
        //Если насекомых нет - идем в случайном направлении
        if (insectsAtSpiderRow.isEmpty() && insectsAtSpiderCol.isEmpty()){
            int direction = rand.nextInt(4);
            switch (direction) {
                case 0:
                    controllable.makeMove(Direction.east());
                    break;
                case 1:
                    controllable.makeMove(Direction.north());
                    break;
                case 2:
                    controllable.makeMove(Direction.south());
                    break;
                case 3:
                    controllable.makeMove(Direction.west());
                    break;
                default:
                    break;
            }
        }
        
        //Если насекомые(ое) есть
        else{
            Insect closestInsectAtRow=null;
            Insect closestInsectAtCol=null;
            int rangeToInsectAtRow=0;
            int rangeToInsectAtCol=0;

            //Вычисляем ближайших насекомых...
            //Находим ближайшее насекомое на одной строке с пауком
            for (Insect ins : insectsAtSpiderCol){
                if (closestInsectAtCol==null){
                    closestInsectAtCol = ins;
                    rangeToInsectAtCol = Math.abs(ins.position().row()-controllable.position().row());
                }
                else if (Math.abs(ins.position().row()-controllable.position().row())<rangeToInsectAtCol){
                    closestInsectAtCol = ins;
                    rangeToInsectAtCol = Math.abs(ins.position().row()-controllable.position().row());
                }
            }

            //Аналогично находим ближайшее насекомое на одном столбце с пауком
            for (Insect ins : insectsAtSpiderRow){
                if (closestInsectAtRow==null){
                    closestInsectAtRow = ins;
                    rangeToInsectAtRow = Math.abs(ins.position().column()-controllable.position().column());
                }
                else if (Math.abs(ins.position().column()-controllable.position().column())<rangeToInsectAtRow){
                    closestInsectAtRow = ins;
                    rangeToInsectAtRow = Math.abs(ins.position().column()-controllable.position().column());
                }
            }

            //Вычисляем, к какому насекомому из найденных пойти
            boolean eatAtRow=false;
            boolean eatAtCol=false;
            //Если насекомое на одном расстоянии и в одном столбце и в одной строке, случайным образом выберем, кого съесть
            if (closestInsectAtCol!=null && closestInsectAtRow!=null && rangeToInsectAtCol==rangeToInsectAtRow){
                if (Math.random() < 0.5)
                    eatAtRow=true;
                else
                    eatAtCol=true;
            }

            //Если существует только насекомое в одном столбце, либо оно ближе, чем насекомое на одной строке, либо выбрано случайным образом 
            if (eatAtCol || (closestInsectAtCol!=null && (closestInsectAtRow==null || rangeToInsectAtCol<rangeToInsectAtRow))){
                if (closestInsectAtCol.position().row()-controllable.position().row()<0)
                    controllable.makeMove(Direction.north());
                else
                    controllable.makeMove(Direction.south());
            }
            //Если существует только насекомое на одной строке, либо оно ближе, чем насекомое на одном столбце, либо выбрано случайным образом
            else if (eatAtRow || (closestInsectAtRow!=null && (closestInsectAtCol==null ||rangeToInsectAtRow<rangeToInsectAtCol))){
                if (closestInsectAtRow.position().column()-controllable.position().column()<0)
                    controllable.makeMove(Direction.west());
                else
                    controllable.makeMove(Direction.east());
            }
        }
    }
    
    /** Установить модель игры
     * @param model модель игры
    */
    public void setModel (GameModel model){
        _model = model;
    }
}
