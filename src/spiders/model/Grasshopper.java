package spiders.model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import spiders.model.navigation.NodePosition;
import spiders.view.GamePanel;

/** Насекомое кузнечик.
 * При успешной попытке вырваться из паутины может вернуть обратно, при этом шанс вырваться из паутины в следующий раз снижается;
 * не попадает на паутину, если на ней уже есть 3 кузнечика
*/
public class Grasshopper extends Insect{
    //Перепрыгивал ли кузнечик с одного узла паутины на другой
    private boolean _leaped=false;
    /** Породить кузнечика
    */
    public Grasshopper(){
        _size=4;
        super.setIcon("grasshopper.png");
    }
    
    /** Понизить множитель шанса вырваться из паутины
    */
    private void decreaseEscapeChanceMultiplier(){
        _escapeChanceMultiplier/=2;
    }
    
    @Override
    /** Попытаться выпрыгнуть из паутины
    */
    public boolean tryEscape(){
        Web oldWeb = _web;
        boolean jumped = super.tryEscape();
        //С вероятностью 50% вернуться на паутину
        if (jumped && Math.random()<0.5){
            setWeb(oldWeb);
            _web.addWebDweller(this, _web.randomBlankNodePosition());
            _leaped=true;
            //Уменьшить шанс вырваться из паутины в следующий раз
            decreaseEscapeChanceMultiplier();
            return false;
        }
        return jumped;
    }
    
    /** Рассчитать возможность попадания на паутину для кузнечика
     * @return true - попасть на паутину можно, false - попасть на паутину нельзя
    */
    @Override
    protected boolean calculatePossibilityToArrive(){
        //Посчитать количество других кузнечиков на паутине
        int numberOfGrasshoppersAtWeb=0;
        for (WebDweller dweller : _web.dwellers()){
            if (dweller instanceof Grasshopper)
                numberOfGrasshoppersAtWeb++;
        }
        //Если кузнечиков меньше 3 и стандартные для всех насекомых условия соблюдены - попасть на паутину можно
        return (super.calculatePossibilityToArrive() && numberOfGrasshoppersAtWeb<3);
    }
    
    /** Отрисовать обитателя паутины
     * @param g класс, обслуживающий графику
     * @param CELL_SIZE размер клетки
     * @param FONT_HEIGHT размер шрифта
     * @param panel панель, на которой отрисовывать
     * @param leftTop точка, указывающая позицию левого верхний угол узла паутины, на котором находится обитатель
     */
    public void draw(Graphics g, Point leftTop, int CELL_SIZE, int FONT_HEIGHT, GamePanel panel) {
        if (!_leaped)
            super.draw(g, leftTop, CELL_SIZE, FONT_HEIGHT, panel);
        //Если обитатель - пытавшийся выпрыгнуть кузнечик, применить соответствующую иконку
        else if (_leaped) {
            File sourceimage = null;
            Image image = null;
            sourceimage = new File("jumpedGrasshopper.png");
            try {
                image = ImageIO.read(sourceimage);
            } catch (IOException ex) {
                Logger.getLogger(GamePanel.class.getName()).log(Level.SEVERE, null, ex);
            }
            g.drawImage(image, leftTop.x + CELL_SIZE / 8 + 27, leftTop.y + CELL_SIZE / 4 + FONT_HEIGHT + 70, panel);
        }
    }
}
