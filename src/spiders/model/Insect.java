package spiders.model;

import spiders.model.navigation.NodePosition;

/** Насекомое.
 * Обитатель паутины; не может передвигаться по паутине; является едой паука; может попасть на паутину и вырваться из неё;
 * отдает определяемый размером уровень здоровья пауку, когда тот его съедает
 */
public abstract class Insect extends WebDweller implements Cloneable{
    //Размер
    protected int _size=1;
    //Базовый множитель шанса вырваться из паутины
    protected double _escapeChanceMultiplier=1;
    /** Попытаться вырваться из паутины
     * @return true - попытка успешка, false - попытка безуспешна
    */
    public boolean tryEscape(){
        //С шансом 0.1*size
        if (Math.random()<(_size*0.05)/(_size*0.05+0.7) && !super.isDead()){
            //Вырваться из паутины
            _web.deleteWebDweller(this);
            return true;
        }
        return false;
    }
    
    /** Вернуть размер
     * @return размер
    */
    public int size(){
        return _size;
    } 
    
    /** Попытаться попасть в паутину
     * @return true - насекомое попало в паутину, false - насекомое не попало в паутину
    */
    public boolean tryToGetIntoTheWeb(){
        NodePosition pos;
        //Если попасть на паутину можно
        if(calculatePossibilityToArrive()==true){
            //Посчитать позицию для попадания
            pos=calculatePositionToArrive();
            _web.addWebDweller(this, pos);
            return true;
        }
        else
            return false;    
    }
    
    /** Дать пауку количество здоровья, определенное размером насекомого
     * @param spider паук
    */
    void giveHealthToSpider(Spider spider){
        spider.increaseHealth(_size*2);
    }
    
    /** Рассчитать позицию попадания на паутину
     * @return NodePosition - позиция попадания на паутину
    */
    protected NodePosition calculatePositionToArrive(){
        NodePosition posToArrive;
        //Позиция попадания - случайный из пустых узлов паутины
        posToArrive=_web.randomBlankNodePosition();
        return posToArrive;
    }
    
    /** Рассчитать возможность попадания на паутину
     * @return true - попасть на паутину можно, false - попасть на паутину нельзя
    */
    protected boolean calculatePossibilityToArrive(){
        //Если на паутине есть хотя бы одна пустая позиция, стандартное насекомое может в неё попасть
        NodePosition possiblePositionToArrive=_web.randomBlankNodePosition();
        
        //Чем больше насекомое, тем меньше шанс попасть на паутину
        return (Math.random()<1/(float)(_size) && possiblePositionToArrive!=null);
    }
    
    /** Клонировать
     * @return Insect - результат клонирования
    */
    public Insect clone(){
        Insect obj = null;
        try {
            obj = (Insect)super.clone();
        } catch (CloneNotSupportedException ex) {}
        return obj;
    }
}
