package spiders.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import spiders.model.navigation.Direction;
import spiders.model.navigation.NodePosition;

/** Паутина.
 * Содержит в себе обитателей паутины; может удалять обитателей и добавлять новых;
 * ловит насекомых; 
*/
public class Web {
    private int _width;
    private int _height;
    private List<WebDweller> _dwellers;
    
    /** Порождает паутину заданных размеров
     * @param width - ширина
     * @param height - высота
    */
    public Web(int width, int height){
        _dwellers = new ArrayList<WebDweller>();
        _width=width;
        _height=height;
    }
    
    /** Добавить обитателя паутины
     * @param dweller - обитатель
     * @param pos - позиция для добавления
     * @return true - добавление успешно, false - добавление безуспешно
    */
    public boolean addWebDweller(WebDweller dweller, NodePosition pos){
        if (dweller(pos)==null && pos.column()<_width && pos.row()<_height){
            dweller.setWeb(this);
            dweller.setPosition(pos);
            _dwellers.add(dweller);
            return true;
        }
        return false;
    }
    
    /** Удалить обитателя паутины
     * @param dweller - обитатель
    */
    public void deleteWebDweller(WebDweller dweller){
        dweller.setWeb(null);
        dweller.setPosition(null);
        _dwellers.remove(dweller);
    }
    
    /** Вернуть список обитателей
     * @return неизменяемый список обитателей паутины
    */
    public List<WebDweller> dwellers(){
        return Collections.unmodifiableList(_dwellers);
    }
    
    /** Вернуть ширину поля
     * @return ширина поля
    */
    public int width(){
        return _width;
    }
    
    /** Вернуть высоту поля
     * @return высота поля
    */
    public int height(){
        return _height;
    }
    
    /** Вернуть список позиций узлов
     * @return список позиций узлов
    */
    public List<NodePosition> nodesPositions(){
        List<NodePosition> nodesPositions = new ArrayList<NodePosition>();
        for (int x = 0; x < _width; x++){
            for (int y = 0; y < _height; y++){
                NodePosition currentPos = new NodePosition(x,y);
                nodesPositions.add(currentPos);
            }
        }   
        return nodesPositions;
    }
    
    /** Рассчитать новую позицию основываясь на текущей позиции и направлении
     * @param oldPos старая позиция
     * @param direct направление
     * @return новая позиция
    */
    public NodePosition calcNewPos(NodePosition oldPos, Direction direct){
        NodePosition newPos;
        if (direct.equals(Direction.south()))
            newPos = new NodePosition(oldPos.row()+1, oldPos.column());
        else if (direct.equals(Direction.east()))
             newPos = new NodePosition(oldPos.row(), oldPos.column()+1);
        else if (direct.equals(Direction.west()))
             newPos = new NodePosition(oldPos.row(), oldPos.column()-1);
        else
             newPos = new NodePosition(oldPos.row()-1, oldPos.column());
        
        //Если такой позиции на паутине нет, вернуть пустой указатель
        if (newPos.column()>_width-1 || newPos.row()>_height-1)
            newPos=null;
        
        return newPos;
    }
    
    /** Вернуть центральную позицию паутины
     * @return центральная позиция
    */
    public NodePosition centralPosition(){
        return new NodePosition((_height/2),(_width/2));
    }
    
    /** Вернуть случайный пустой узел
     * @return случайный пустой узел
    */
    public NodePosition randomBlankNodePosition(){
        Random rand = new Random();
        List<NodePosition> nodesPositions = nodesPositions();
        Iterator<NodePosition> iter;
        //Удалить из списка все занятые позиции
        for (WebDweller dweller : _dwellers){
            iter = nodesPositions.iterator();
            while (iter.hasNext()){
                if (dweller.position().equals(iter.next()))
                    iter.remove();
            }
        }
        if (!nodesPositions.isEmpty())
            //Возвратить случайную позицию
            return nodesPositions.get(rand.nextInt(nodesPositions.size()));
        else 
            return null;
    } 
    
    /** Очистить паутину от обитателей
    */
    public void clear(){
        ListIterator<Insect> dwellerIter = ((ArrayList)_dwellers).listIterator();
        while (dwellerIter.hasNext()){
            dwellerIter.next();
            dwellerIter.remove();
        }
    }
    
    /** Вернуть обитателя на заданной
     * @param pos позиция
     * @return обитатель на заданной позиции, null - если на позиции никого нет
    */
    public WebDweller dweller (NodePosition pos){
        for (WebDweller dweller : _dwellers){
            if (dweller.position().equals(pos))
                return dweller;
        }
        return null;
    }
    
}
