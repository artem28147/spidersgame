package spiders.model;

/** Насекомое муха.
 * Простое насекомое
 */
public class Fly extends Insect{
    /** Породить муху
    */
    public Fly(){
        _size=2;
        //Задать иконку для отображения на паутине
        super.setIcon("fly.png");
    }
}
