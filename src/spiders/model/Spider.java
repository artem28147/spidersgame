package spiders.model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import spiders.model.events.SpiderActionEvent;
import spiders.model.events.SpiderActionListener;
import spiders.model.navigation.Direction;
import spiders.model.navigation.NodePosition;
import spiders.view.GamePanel;

/** Паук.
 * Обитатель паутины; может передвигаться по паутине; при каждом шаге уровень здоровья понижается на 1
 * пополняет уровень здоровья, питаясь насекомыми; умирает, когда уровень здоровья становится ниже единицы
 */
public class Spider extends WebDweller{
    //Уровень здоровья
    private int _health=25;
    private boolean _isPlayer=false;
    private int _passedStepsCount=0;
    /** Породить паука
    */
    public Spider(){
        super.setIcon("botspider.png");
    }
    
    /** Установить, является ли паук пауком игрока
     * @param isPlayer является ли паук пауком игрока
    */
    public void setIsPlayer(boolean isPlayer){
        _isPlayer=isPlayer;
    }
    
    /** Съесть насекомое
     * @param victim насекомое
    */
    private void eat(Insect victim){
        //Уровень здоровья до поедания насекомого
        int healthBeforeEating=_health;
        //Уровень здоровья после поедания насекомого
        int healthAfterEating;
        //Уровень здоровья, потоерянный пауком при попытке съесть насекомое
        int lostHealth;
        
        //Сигнализировать о том, что паук собирается съесть насекомое
        fireTryingToAteInsect(victim);
        lostHealth=healthBeforeEating-_health;
        //Если паук не умер, жертва пополняет его уровень здоровья и умирает
        if(_health>0){
            victim.giveHealthToSpider(this);
            victim.die();
        }
        
        healthAfterEating=_health;
        //Просигнализировать о съеденном насекомом, указав полученный уровень здоровья
        fireAteInsect(healthAfterEating-healthBeforeEating+lostHealth, lostHealth);
    }
    
    /** Повысить количество пройденных шагов на 1
    */
    public void increasePassedStepsCount(){
        _passedStepsCount++;
    }
    
    /** Вернуть количество пройденных шагов
     * @return количество пройденных шагов
    */
    public int passedStepsCount(){
        return _passedStepsCount;
    }
    
    /** Вернуть уровень здоровья
     * @return уровень здоровья
    */
    public int health(){
        return _health;
    }
    
    /** Уменьшить уровень здоровья
     * @param number количество
    */
    void reduceHealth(int number){
        _health-=number;
    }
    
    /** Увеличить уровень здоровья
     * @param number количество
    */
    void increaseHealth(int number){
      _health+=number;  
    }
    
    //Список слушателей действий паука
    private ArrayList<SpiderActionListener> _listeners = new ArrayList();
    
    /** Добавить слушателя действий паука
     * @param l слушатель
    */
    public void addSpiderActionListener(SpiderActionListener l) {
        _listeners.add(l);
    }
    
    /** Удалить слушателя действий паука
     * @param l слушатель 
    */
    public void removeSpiderActionListener(SpiderActionListener l){
        _listeners.remove(l);
    }
    
    /** Сигнализировать о том, что паук сделал ход
    */
    protected void fireSpiderMakedMove() {
        SpiderActionEvent event = new SpiderActionEvent(this);
        List<SpiderActionListener> currentList = Collections.synchronizedList(new ArrayList(_listeners));
        for (SpiderActionListener listener : currentList)
            listener.spiderMakedMove((SpiderActionEvent) event);   
    }
    
    /** Сигнализировать о том, что паук пытается съесть насекомое
     * @param victim жертва
    */
    protected void fireTryingToAteInsect(Insect victim) {
        SpiderActionEvent event = new SpiderActionEvent(this);
        event.setVictim(victim);
        event.setSpider(this);
        for (SpiderActionListener listener : _listeners)
            listener.spiderTryingToEatInsect(event);  
    }
    
    /** Сигнализировать о том, что паук съел насекомое
     * @param receivedHealth уровень полученного здоровья
     * @param lostHealth уровень потерянного здоровья (от осы)
    */
    protected void fireAteInsect(int receivedHealth, int lostHealth) {
        SpiderActionEvent event = new SpiderActionEvent(this);
        event.setReceivedHealth(receivedHealth);
        event.setLostHealth(lostHealth);
        for (SpiderActionListener listener : _listeners)
            listener.spiderAteInsect(event);   
    }
    
    /** Сделать ход
     * @param direct направление для хода
    */
    public void makeMove(Direction direct){
        NodePosition newPos = _web.calcNewPos(super.position(), direct);
        List<NodePosition> existingPositions = _web.nodesPositions();
        //Узнать, существует ли новая позиция
        boolean newPositionExists=false;
        for (NodePosition pos : existingPositions){
            if (pos.equals(newPos))
                newPositionExists=true;
        }
        
        //Проверить новую позицию на наличие других пауков или насекомых
        List<WebDweller> webDwellers = _web.dwellers();
        boolean noSpiderAtPos=true;
        boolean insectAtPos=false;
        WebDweller insect = null;
        if (_web.dweller(newPos) instanceof Spider)
            noSpiderAtPos=false;
        else if (_web.dweller(newPos) instanceof Insect){
            insectAtPos=true;
            insect=_web.dweller(newPos);
        }
        
        
        //Если позиция существует и на ней нет другго паука
        if (newPositionExists && noSpiderAtPos){
            setPosition(newPos);

            //Если есть насекомое - съесть его
            if (insectAtPos){
                eat((Insect)insect);
            }

            //Если паук не умер - уменьшить здоровье на единицу после хода
            if (!isDead()){
                reduceHealth(1);
                if (_health<1){
                    _health=0;
                    die();
                }
            }

            //Сигнализировать о ходе паука
            fireSpiderMakedMove();
        }
    }
    
    /** Отрисовать обитателя паутины
     * @param g класс, обслуживающий графику
     * @param CELL_SIZE размер клетки
     * @param FONT_HEIGHT размер шрифта
     * @param panel панель, на которой отрисовывать
     * @param leftTop точка, указывающая позицию левого верхний угол узла паутины, на котором находится обитатель
     */
    public void draw(Graphics g, Point leftTop, int CELL_SIZE, int FONT_HEIGHT, GamePanel panel) {
        if(!_isPlayer)
            super.draw(g, leftTop, CELL_SIZE, FONT_HEIGHT, panel);
        
        //Если паук-игрок, отрисовать другую иконку
        else if(_isPlayer){
            File sourceimage = null;
            Image image = null;
            sourceimage = new File("playerSpider.png");
            try {
                image = ImageIO.read(sourceimage);
            } catch (IOException ex) {
                Logger.getLogger(GamePanel.class.getName()).log(Level.SEVERE, null, ex);
            }
            g.drawImage(image, leftTop.x + CELL_SIZE / 8 + 27, leftTop.y + CELL_SIZE / 4 + FONT_HEIGHT + 70, panel);
            //Отрисовать уровень здоровья паука игрока
            g.setColor(Color.WHITE);   
            g.drawString("Здоровье: " + String.valueOf(_health), 10, 47);
            // Отрисовать количество пройденных шагов
            g.drawString("Пройдено шагов: " + String.valueOf(_passedStepsCount), 100, 47);
        }
        
        //Отрисовать полосу жизни
        int lineLength = ((Spider) this).health() <= 30 ? ((Spider) this).health() : 30;
        g.setColor(Color.RED);
        g.fillRect(leftTop.x + CELL_SIZE / 8 + 30, leftTop.y + CELL_SIZE / 4 + FONT_HEIGHT + 102, lineLength, 2);
        g.setColor(Color.WHITE);
        g.fillRect(leftTop.x + CELL_SIZE / 8 + 30 + lineLength, leftTop.y + CELL_SIZE / 4 + FONT_HEIGHT + 102, 30 - lineLength, 2);
        g.setColor(Color.BLACK);
    }
}
