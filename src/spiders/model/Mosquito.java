package spiders.model;

/** Насекомое комар.
 * Простое насекомое
 */
public class Mosquito extends Insect{
    /** Породить комара
    */
    public Mosquito(){
        _size=1;
        super.setIcon("mosquito.png");
    }
}
