package spiders.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JPanel;
import spiders.model.GameModel;
import spiders.model.navigation.Direction;
import spiders.model.navigation.NodePosition;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Box;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import spiders.model.WebDweller;
import spiders.model.events.GameEvent;
import spiders.model.events.GameListener;
import spiders.model.events.SpiderActionEvent;
import spiders.model.events.SpiderActionListener;

/** Игровая панель.
 * Отображает на экране ход игры; реагирует на нажатия игрока по клавиатуре;
 * инициализирует новую игру
*/
public class GamePanel extends JPanel{
    
    //Модель игры
    private GameModel _model = new GameModel();
    
    //Размеры
    private static final int CELL_SIZE = 30;
    private static final int GAP = 2;
    private static final int FONT_HEIGHT = 15;

    //Цветовое оформление
    private static final Color BACKGROUND_COLOR = new Color(57, 178, 63);
    private static final Color GRID_COLOR = Color.WHITE;
    
    //Элементы меню
    private JMenuBar _menu = null;
    private final String fileItems[] = new String []{"Новая игра", "Выход"};
    
    //Переменные, необходимые для корректной обработки событий
    private boolean _gameStarted=false;
    private boolean _playerSpiderAteInsect=false;
    private boolean _playerSpiderWasBited=false;
    private boolean _gameOver=false;
    private int _playerPassedStepsCount;
    //Ссылки, необходимые для отображения полученного и потерянного пауком здоровья при поедании насекомого
    private int _receivedHealth;
    private int _lostHealth;
    //Ссылки, необходимые для отображения перепрыгнувшего кузнечика и паука игрока
    
    private KeyManager _keyManager = new KeyManager();
    
    /** Породить игровую панель
    */
    public GamePanel(){
        _menu = new JMenuBar();
        //Инициализация графики
        int width = 2*GAP + CELL_SIZE * _model.web().width();
        int height = 2*GAP + CELL_SIZE * _model.web().height()+60;
        setPreferredSize(new Dimension(width, height));
        setBackground(Color.RED);
        createMenu();
        add(Box.createRigidArea(new Dimension(width*(-1)+53, 0)));
        add(_menu, BorderLayout.AFTER_LAST_LINE);
    }
    
    /** Отрисовать игровую ситуацию
     * @param g класс, обслуживающий графику
    */
    @Override
    public void paintComponent(Graphics g) {
        //Установить сглаживание и шрифт
        RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        g.setFont(new Font("Arial", Font.BOLD, 12));
        ((Graphics2D)g).setRenderingHints(rh);
        
        int width  = getWidth();
        int height = getHeight();
        
        //Отрисовать фон
        g.setColor(BACKGROUND_COLOR);
        g.fillRect(0, 0, width, height);
        g.setColor(Color.BLACK);
        
        if (_gameStarted && !_gameOver){
            //Отрисовать паутину
            drawWeb(g);
                
            //Отрисовать всех обитателей паутины
            for(WebDweller obj : _model.web().dwellers()){
                obj.draw(g,leftTopOfNode(obj.position()),CELL_SIZE,FONT_HEIGHT, this);
            }
            
            //Отрисовать уровень полученного и потерянного(от осы) здоровья, если паук игрока съел насекомое
            if(_playerSpiderAteInsect){
                Point leftTop = leftTopOfNode(_model.playerSpider().position());
                g.setColor(Color.WHITE);
                g.drawString("+"+String.valueOf(_receivedHealth), leftTop.x+CELL_SIZE/8+45, leftTop.y+CELL_SIZE/4+FONT_HEIGHT+75);
                if (_lostHealth!=0){
                    g.setColor(Color.RED);
                    g.drawString("-"+String.valueOf(_lostHealth), leftTop.x+CELL_SIZE/8+25, leftTop.y+CELL_SIZE/4+FONT_HEIGHT+75);
                    g.setColor(Color.BLACK);
                }
                _playerSpiderAteInsect=false;
            }
            
            if(_playerSpiderWasBited){
                Point leftTop = leftTopOfNode(_model.playerSpider().position());
                g.drawString("+"+String.valueOf(_receivedHealth), leftTop.x+CELL_SIZE/8+45, leftTop.y+CELL_SIZE/4+FONT_HEIGHT+75);
                _playerSpiderAteInsect=false;
            }
            g.setColor(Color.BLACK);
        }
    }
    
    /** Отрисовать паутину
     * @param g класс, обслуживающий графику
    */
    private void drawWeb(Graphics g) {
        int width  = getWidth();
        int height = getHeight()+40;

        g.setColor(GRID_COLOR);
        //Вертикальные линии
        for(int i = 1; i <= _model.web().width()+1; i++)  
        {
            int x = GAP + CELL_SIZE*(i-1)+18;
            g.drawLine(x, 60, x, height);
        }
        //Горизотнальные линии
        for(int i = 1; i <= _model.web().width(); i++) 
        {
            int y = GAP + CELL_SIZE*(i-1)+80;
            g.drawLine(0, y, width, y);
        }
    }
    
    /** Вернуть позицию левого верхнего угла узла паутины
     * @param pos позиция узла паутины
     * @return позиция левого верхнего угла узла паутины
    */
    private Point leftTopOfNode(NodePosition pos) {
        
        int left = GAP + CELL_SIZE * (pos.column()-1);
        int top = GAP + CELL_SIZE * (pos.row()-1);
        
        return new Point(left, top);
    }

    /** Реакция на действие паука игрока
    */
    private class RepaintBySpiderAction implements SpiderActionListener {
        /** Отреагировать на событие "паук съел насекомое"
         * @param e событие
        */
        @Override
        public void spiderAteInsect(SpiderActionEvent e) {
           //Установить флаг того, что паук съел насккомое
           _playerSpiderAteInsect=true;
           //Установить уровень полученного пауком здововья
           _receivedHealth=e.receivedHealth();
           _lostHealth=e.lostHealth();
        }
        
        @Override
        public void spiderMakedMove(SpiderActionEvent e) {
        }
        
        @Override
        public void spiderTryingToEatInsect(SpiderActionEvent e) {
        }
    }
    
    /** Реакция на событие от модели игры
    */
    private class DoByGameModelEvent implements GameListener{

        /** Отреагировать на событие "конец игры"
         * @param e событие
        */
        @Override
        public void gameOver(GameEvent e) {
            _playerPassedStepsCount=e.playerPassedStepsCount();
           //Установить флаг того, что игра окончена
           _gameOver=true;
           //Отрисовать конец игры
           paintGameOver();
        }

        /** Отреагировать на событие "ситуация на паутине изменилась"
         * @param e событие
        */
        @Override
        public void webSituationChanged(GameEvent e) {
            repaint();
        }
    }
    
    /** Отрисовать окончание игры
    */
    private void paintGameOver() {
        //Перерисовать обстановку на паутине
        repaint();
        //Отрисовать окошко с результатом игры
        String str = "Ваш счет: " + _playerPassedStepsCount + " шагов";
        JOptionPane.showMessageDialog(null, str, "Игра завершена!", JOptionPane.DEFAULT_OPTION);
    }
    
    /** Создать меню
    */
    private void createMenu() {
        _menu = new JMenuBar();
        JMenu fileMenu = new JMenu("Меню");

        //Добавить пункты меню
        for (int i = 0; i < fileItems.length; i++) {
           
            JMenuItem item = new JMenuItem(fileItems[i]);
            item.setActionCommand(fileItems[i].toLowerCase());
            item.addActionListener(new NewMenuListener());
            fileMenu.add(item);
        }
        fileMenu.insertSeparator(1);

        _menu.add(fileMenu);
    }

    /** Реакция на нажатие по пункту меню
    */
    public class NewMenuListener implements ActionListener {
        /** Отреагировать на событие нажатия по пункту меню
         * @param e событие
        */
        @Override
        public void actionPerformed(ActionEvent e) {
            String command = e.getActionCommand();
            if ("выход".equals(command)) {
                System.exit(0);
            }
            if ("новая игра".equals(command)) {
                startGame();
            }  
        }
    }
    
    /** Инициализация запуска новой игры
    */
    private void startGame(){ 
        //Если игра начинается в первый раз, добавить слушателя кнопок и события окончания игры
        if (!_gameStarted){
            _model.addGameListener(new GamePanel.DoByGameModelEvent());
            this.addKeyListener(_keyManager);
            _gameStarted=true;
        }
        
        _model.start();
        _model.playerSpider().addSpiderActionListener(new GamePanel.RepaintBySpiderAction());
        
        _gameOver=false;
        
        //Отрисовать игровую ситуацию
        repaint();
    }
    
    /** Класс, отвечающий за реакцию на нажатие клавиш
    */
        private class KeyManager extends KeyAdapter{
        /** Реагировать на нажатие клавиши
        * @param ke нажатая клавишиа
        */
        @Override
        public void keyPressed(KeyEvent ke) {
            if (!_gameOver){
                switch (ke.getKeyCode()) {
                    case KeyEvent.VK_UP:
                        _model.playerSpider().makeMove(Direction.north());
                        break;
                    case KeyEvent.VK_DOWN:
                        _model.playerSpider().makeMove(Direction.south());
                        break;
                    case KeyEvent.VK_LEFT:
                        _model.playerSpider().makeMove(Direction.west());
                        break;
                    case KeyEvent.VK_RIGHT:
                        _model.playerSpider().makeMove(Direction.east());
                        break;
                    default:
                        break;
                }
            }
        }
    };
}