package spiders;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import spiders.model.GameModel;
import spiders.view.GamePanel;

/** Главный класс игры "Пауки"
 */
public class Spiders extends JFrame{
    
    //Модель игры
    private GameModel _model;
    //Игровая панель
    private GamePanel _gamePanel;
    
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new Spiders();
            }
        });
    }
    
    /** Конструктор класса игры
    */
    public Spiders(){
        //Инициализация панели
        _gamePanel = new GamePanel();
        
        //Добавление игровой панели на JPanel
        JPanel content = new JPanel();
        content.setLayout(new BorderLayout());
        content.add(_gamePanel, BorderLayout.CENTER);
        
        //Установка параметров игрового окна
        setContentPane(content);
        setTitle("Пауки");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);

        _gamePanel.setFocusable(true);
        _gamePanel.setVisible(true);        
    }
}
